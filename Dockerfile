FROM dorowu/ubuntu-desktop-lxde-vnc

ADD src /app

RUN apt-get update \
    && apt-get install -y netatalk libc6-dev avahi-daemon libnss-mdns \
    && chmod +x /app

EXPOSE 548/tcp
EXPOSE 5353/udp

ENTRYPOINT ["/bin/sh", "/app/setup.sh"]