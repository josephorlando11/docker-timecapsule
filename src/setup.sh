#!/usr/bin/env sh

hostname "$HOSTNAME"
echo "$HOSTNAME" > /etc/hostname

useradd -p "$PASSWORD" "backup"
echo "backup:$PASSWORD" | chpasswd

mkdir -p /mnt/backup
chown backup:backup /mnt/backup
chmod 775 /mnt/backup

cd /app
cp nsswitch.conf /etc
cp afpd.service /etc/avahi/services/afpd.service
cp AppleVolumes.default /etc/netatalk/AppleVolumes.default
echo "/mnt/backup \"TimeMachine\" options:tm volsizelimit:$VOLSIZELIMIT allow:backup" >> /etc/netatalk/AppleVolumes.default
echo "\n-setuplog \"default log_debug /var/log/afpd.log\" -hostname \"$HOSTNAME\"" >> /etc/netatalk/afpd.conf

service dbus restart
service netatalk restart
service avahi-daemon restart

tail -f /dev/null