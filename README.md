# TimeMachine Server for Docker!
![alt text](https://gitlab.com/josephorlando11/docker-timecapsule/badges/master/pipeline.svg "Pipeline Status")

Initializes netatalk + avahi-daemon services to mount-point `/mnt/backup`.

It's a quick-and-scrappy implementation for personal use.

### Environment Variables (required)
```
VOLSIZELIMIT=<volume size shown to clients>
PASSWORD=<backup user password>
HOSTNAME=<server hostname>
```

### How to run (via DockerHub)
```
docker run --cap-add SYS_ADMIN \
           --privileged \
           -e PASSWORD="<backup user password>" \
           -v /local/storage:/mnt/backup \
           joeyx22lm/docker-timecapsule
```

### How to connect
`afp://backup:<password>@<container ip>`